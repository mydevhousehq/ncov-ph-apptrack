import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestService } from '../rest.service';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  public datax : any = [];
  public data1 : any = [];
  public data2 : any = [];
  public data3 : any = [];
  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.rest.getDataxx("cases").subscribe((data: {}) => {
      console.log(data);
      this.datax = data;
    });
  }


}
