// import { Injectable } from '@angular/core';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import {environment } from '../environments/environment';
const endpoint = environment.ApiUrl;
@Injectable({
  providedIn: 'root'
})

export class RestService {
 
  constructor(private http: HttpClient) { 
   
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };


    
  }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  getDataxx(id): Observable<any> {
    return this.http.get(endpoint  + id).pipe(
      map(this.extractData));
  }

}
